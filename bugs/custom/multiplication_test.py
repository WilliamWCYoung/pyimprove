import unittest

from multiplication import multiplication


class MultiplicationTest(unittest.TestCase):
    def test_left(self):
        self.assertEqual(multiplication(3, 1), 3)

    def test_right(self):
        self.assertEqual(multiplication(1, 3), 3)

    def test_same(self):
        self.assertEqual(multiplication(3, 3), 9)

    def test_zero(self):
        self.assertEqual(multiplication(0, 0), 0)

    def test_leftzero(self):
        self.assertEqual(multiplication(0, 100), 0)
