import unittest
import math

from factorial import factorial


class FactorialTest(unittest.TestCase):
    def test_0(self):
        self.assertEqual(factorial(0), 1)

    def test_1(self):
        self.assertEqual(factorial(1), 1)

    def test_n(self):
        for n in range(2, 10):
            self.assertEqual(factorial(n), math.factorial(n))
