import unittest

from multi_typo import multi_typo


class MultiTypoTest(unittest.TestCase):
    def test_left(self):
        self.assertEqual(multi_typo(3, 1), 3)

    def test_right(self):
        self.assertEqual(multi_typo(1, 3), 3)

    def test_same(self):
        self.assertEqual(multi_typo(3, 3), 9)

    def test_zero(self):
        self.assertEqual(multi_typo(0, 0), 0)

    def test_leftzero(self):
        self.assertEqual(multi_typo(0, 100), 0)
