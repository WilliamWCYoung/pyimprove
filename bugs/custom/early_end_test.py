import unittest

from early_end import early_end


class EarlyEndTest(unittest.TestCase):
    def test_base(self):
        self.assertEqual(early_end(1), 1)

    def test_further(self):
        self.assertEqual(early_end(2), 2)


if __name__ == '__main__':
    unittest.main()
