import unittest

from large import Large


class LargeTest(unittest.TestCase):
    def test_0(self):
        instance = Large(1)

        self.assertEqual(instance.double(), 2)

    def test_1(self):
        instance = Large(1)

        self.assertEqual(instance.equal(1), True)

    def test_2(self):
        instance = Large(1)

        self.assertEqual(instance.equal(2), False)
