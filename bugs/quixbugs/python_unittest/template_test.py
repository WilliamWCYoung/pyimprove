import unittest
import json
import os
import timeout_decorator

from bugs.quixbugs.python_unittest.python_programs.{{name}} import {{name}}


class TemplateTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.function = {{name}}
        self.filename = "./json_testcases/{{name}}.json"

    def setUp(self):
        self.test_data = []

        if self.function is None or self.filename is None:
            return

        script_dir = os.path.dirname(__file__)
        abs_file_path = os.path.join(script_dir, self.filename)

        with open(abs_file_path) as f:
            for line in f:
                self.test_data.append(json.loads(line))

    @timeout_decorator.timeout(0.01)
    def call_function(self, input_data):
        if isinstance(input_data, list):
            return self.function(*input_data)

        return self.function(input_data)

    def test_json(self):
        for input_data, output_data in self.test_data:
            with self.subTest(input_data=input_data, output_data=output_data):
                try:
                    result = self.call_function(input_data)
                except timeout_decorator.TimeoutError:
                    result = None

                self.assertEqual(result, output_data)


if __name__ == "__main__":
    unittest.main()
